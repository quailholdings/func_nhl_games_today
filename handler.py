import json
import urllib2

# this handles potential unicode eg Montreal
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

nhl_api_url = 'https://statsapi.web.nhl.com'

def handle(st):
    todays_games = json.load(urllib2.urlopen("https://statsapi.web.nhl.com/api/v1/schedule"))
   # print(todays_games)



    number_games = todays_games["dates"][0]["totalGames"]
    #print('Number of games tonight: {}'.format(number_games) )
    game_summary = []
    for x in todays_games["dates"][0]["games"]:
        game = {}
        game['HomeTeam'] = byteify(x['teams']['home']['team']['name'])
        game['AwayTeam'] = byteify(x['teams']['away']['team']['name'])
        game['Description'] = '{} at {}'.format(game['AwayTeam'],game['HomeTeam'])
        game['url'] = '{}{}'.format(nhl_api_url ,x["link"])
        game_summary.append(game)

    print '{"games": %s}' %json.dumps(game_summary, sort_keys=True, indent=4,
                  separators=(',', ': '))

if __name__ == "__main__":
    handle('test')